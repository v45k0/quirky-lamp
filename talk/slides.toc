\select@language {ngerman}
\select@language {english}
\beamer@endinputifotherversion {3.36pt}
\select@language {ngerman}
\select@language {ngerman}
\select@language {english}
\beamer@subsectionintoc {0}{1}{Quirky Lamp}{2}{0}{0}
\beamer@sectionintoc {1}{Part I - Sensor Data Collection}{6}{0}{1}
\beamer@subsectionintoc {1}{1}{Sensor Data and MQTT}{7}{0}{1}
\beamer@sectionintoc {2}{Part II - Sound-Lightning Service}{9}{0}{2}
\beamer@sectionintoc {3}{Video Demo}{12}{0}{3}
\beamer@sectionintoc {4}{Interesting Obstacles}{13}{0}{4}
\beamer@subsectionintoc {4}{1}{ADC Accuracy}{14}{0}{4}
\beamer@subsectionintoc {4}{2}{Polling vs Interrupts}{15}{0}{4}

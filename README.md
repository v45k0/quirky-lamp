# Quirky Lamp

------------------------------------

## Table of Contents
- [Description](#description)
- [Resources](#resources)
- [Compiling](#compiling)
- [License](#license)

------------------------------------

### Description
Quirky Lamp is a simple IoT powered
lamp that was created as part of a student project for the Real-Time Systems
lecture, given by Dr.Alexander Lenz at the
[Technical University of Munich](http://tum.de)
during the 2017/18 Winter Term.

It utilizes the Wi-Fi and Bluetooth
capable ESP32 system on chip (SoC). It collects data from a photo resistor, sound
and temperature sensors and distributes it via MQTT. A multicoloured lightning
from an LED strip provides the basic lamp functionality. In addition to that, the
lamp is capable of dynamically adjusting its lightning based on sound noises (e.g.
reacting in real time on music).

Check the [documentation](https://gitlab.com/v45k0/quirky-lamp/raw/master/report/quirky-lamp-report.pdf) for more information and demo links.

The presentation [slides](https://gitlab.com/v45k0/quirky-lamp/raw/master/talk/slides.pdf) are also [available](https://gitlab.com/v45k0/quirky-lamp/raw/master/talk/slides.pdf).

### Resources

- [Documentation/Report](https://gitlab.com/v45k0/quirky-lamp/raw/master/report/quirky-lamp-report.pdf) - check for more detailed information
- [Hardware Schematics](https://gitlab.com/v45k0/quirky-lamp/raw/master/hardware-schematics/quirky-lamp.pdf)
- [Presentation Slides](https://gitlab.com/v45k0/quirky-lamp/raw/master/talk/slides.pdf)

### Compiling

Before proceeding, make sure to make to get yourself
familiar with [PlatformIO](http://platformio.org/).
Then install [PlatformIO Core](http://docs.platformio.org/en/latest/core.html).

Run/compile/flash the project with:
```
cp flags.py.example flags.py # populate flags.py with the needed credentials
pio run --upload-port /dev/ttyUSB0 -t upload # adjust the port
```

### License

The third-party components used in the project are:
- [FastLED](https://github.com/FastLED/FastLED), MIT licensed
- [arduino-mqtt](https://github.com/256dpi/arduino-mqtt), MIT licensed
- [LiquidCrystal](https://github.com/arduino-libraries/LiquidCrystal), LGPL licensed
- [Arduino Core for ESP32](https://github.com/espressif/arduino-esp32), LGPL licensed

The project code itself is released under GNU GPLv3.

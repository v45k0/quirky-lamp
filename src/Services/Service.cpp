#include "Service.h"

Service::Service(String sid, bool active) {
    this->sid = sid;
    this->active = active;
}

const String Service::getSID() {
    return this->sid;
}

const bool Service::isActive() {
    return this->active;
}

void Service::execute() {
    if (! this->active) {
        return;
    }
    this->run();
}

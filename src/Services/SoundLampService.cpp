#include "SoundLampService.h"

SoundLampService::SoundLampService():
    Service(String("Lamp"), true) {
    this->leds = new CRGB[REACTIVE_LEDS_NUM];
    FastLED.addLeds<NEOPIXEL, LED_STRIP_DIN>(this->leds, REACTIVE_LEDS_NUM);
    FastLED.clear(); FastLED.show();

    this->switchState = LOW;
    this->activeMode = 0;
    this->samples = 0;
    this->maximum = -1;
    this->minimum = int(1 << 13);
}

SoundLampService* SoundLampService::singleton = NULL;
SoundLampService* SoundLampService::get() {
    if (SoundLampService::singleton != NULL) {
        return SoundLampService::singleton;
    }
    SoundLampService::singleton = new SoundLampService();
    return SoundLampService::singleton;
}

void SoundLampService::run() {
    bool change = false;
    int button = digitalRead(MODE_SWITCH);
    if (this->switchState == LOW && button == HIGH) {
        this->activeMode = (this->activeMode + 1) % (this->COLOURS.size() + 1);
        this->switchState = HIGH;
    } else if (this->switchState == HIGH && button == LOW) {
        this->switchState = LOW;
        change = true;
    }

    if (this->activeMode == this->COLOURS.size()) {
        int x = analogRead(SOUND_SENSOR_AO);

        if (this->samples < CALIBRATION_SAMPLES) {
            Log.infoln(String(F("Calibration sample = ")) + String(x));
            if (x > this->maximum) this->maximum = x;
            if (x < this->minimum) this->minimum = x;
            samples++;
            if (samples == CALIBRATION_SAMPLES) {
                this->baseLevel = (this->maximum + this->minimum) >> 1;
                Log.infoln(String(F("Found calibration level = ")) + String(this->baseLevel));
            }
            delay(50);
            return;
        }

        int d = x - this->baseLevel;
        if (d < REACTIVE_NOISE_THRESHOLD) {
            FastLED.clear(); FastLED.show();
            return;
        }
        
        Log.debugln(String(F("Sound = ")) + String(x) + String(" delta = ") + String(d));
        d = constrain(d, REACTIVE_NOISE_THRESHOLD, REACTIVE_PEAK_THRESHOLD);
        int len = map(d, REACTIVE_NOISE_THRESHOLD, REACTIVE_PEAK_THRESHOLD, 0,
                REACTIVE_LEDS_NUM - REACTIVE_BASELINE_SIZE);
        len += REACTIVE_BASELINE_SIZE;
        FastLED.clear();
        for (auto i = 0; i < len && i <  REACTIVE_LEDS_NUM / 3; i++)
            this->leds[i] = CRGB::YellowGreen;
        for (auto i = REACTIVE_LEDS_NUM / 3; i < len && i < 2 * REACTIVE_LEDS_NUM / 3; i++)
            this->leds[i] = CRGB::Yellow;
        for (auto i = 2 * REACTIVE_LEDS_NUM / 3; i < len && i < REACTIVE_LEDS_NUM; i++)
            this->leds[i] = CRGB::Red;
        FastLED.show();
    } else if (change) {
        fill_solid(this->leds, REACTIVE_LEDS_NUM, this->COLOURS[this->activeMode]);
        FastLED.show();
    }
}

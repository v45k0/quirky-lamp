#include "NetworkService.h"

NetworkService::NetworkService() :
    Service(String(F("Network")), true) {}

bool NetworkService::isConnected() {
    return (WiFi.status() == WL_CONNECTED && this->mqtt->connected());
}

bool NetworkService::reconnect() {
    if (this->isConnected()) {
        return true;
    }
    if (WiFi.status() == WL_CONNECTED) {
        return this->connectToBroker();
    }
    return (this->connectToNetwork() && this->connectToBroker());
}

void NetworkService::sendPacket(
    const String &topic, const String &payload) {
    auto it = this->packets.find(topic);
    if (it != this->packets.end()) {
        it->second = payload;
    } else {
        this->packets.insert(std::make_pair(topic, payload));
    }
}

NetworkService* NetworkService::singleton = NULL;
NetworkService* NetworkService::get() {
    if (NetworkService::singleton != NULL) {
        return NetworkService::singleton;
    }

    NetworkService::singleton = new NetworkService();
    Log.debugln(F("NetworkService singleton created!"));
    Log.debugln(F("Connecting to the local WiFi network..."));
    if (singleton->connectToNetwork()) {
        singleton->wifi = std::unique_ptr<WiFiClient>(new WiFiClient());
        singleton->mqtt = std::unique_ptr<MQTTClient>(new MQTTClient());
        singleton->mqtt->begin(MQTT_HOST, *singleton->wifi);
        singleton->connectToBroker();
    }
    return NetworkService::singleton;
}

void NetworkService::run() {
    this->mqtt->loop();
    delay(20);

    if (! this->isConnected()) {
        this->reconnect();
    }

    for (auto it = this->packets.begin(); it != this->packets.end(); it++) {
        Log.debugln(String(F("Publishing topic = ")) + it->first);
        Log.debugln(String(F("Publishing payload = ")) + it->second);
        if(! this->mqtt->publish(it->first, it->second)) {
            Log.warningln(String(F("Publishing failed!")));
        }
    }
    this->packets.clear();
}

bool NetworkService::connectToNetwork() {
    int networks = WiFi.scanNetworks();
    if (networks == 0) {
        Log.debugln("No WiFi networks were found!");
        return false;
    }

    // display all found wifi networks
    bool found = false;
    String opened = String("open"), locked = String("locked");
    String target = String(WIFI_SSID), pass = String(WIFI_PASSWORD);
    for (auto i = 0; i < networks; i++) {
        String& protection = locked;
        if (WiFi.encryptionType(i) == WIFI_AUTH_OPEN) {
            protection = opened;
        }
        Log.infoln(String(i + 1) + String(F(": ")) + WiFi.SSID(i) + 
                String(F(" (")) + String(WiFi.RSSI(i)) +
                String(F("db) - ")) + protection);
        if (target.equals(WiFi.SSID(i))) {
            found = true;
        }
        delay(10);
    }

    if (! found) {
        Log.warningln(target + String(F(" was not found!")));
        return false;
    }

    Log.infoln(String(F("Connecting to "))
            + target + String(F(" with the specified pass...")));
    WiFi.begin(target.c_str(), pass.c_str());
    for (auto i = 0; i < 10; i++) {
        delay(500);
        if (WiFi.status() == WL_CONNECTED) {
            Log.infoln(String(F("Connected to ")) + target);
            Log.debugln(String(F("IPv4: ")) + WiFi.localIP().toString());
            return true;
        }
    }
    Log.warningln(String(F("Failed to connect to ")) + target);
    return false;
}

bool NetworkService::connectToBroker() {
    for (auto i = 0; i < 10; i++) {
        delay(500);
        if (this->mqtt->connect(MQTT_ID, MQTT_USER, MQTT_PASSWORD)) {
            return true;
        }
        Log.debugln("Failed to connect to the mqtt broker");
    }
    return false;
}

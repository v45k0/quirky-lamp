#ifndef SERVICE_H
#define SERVICE_H

#include <WString.h>

class Service {
    protected:
        String sid;
        bool active;
        Service(String sid, bool active = true);
        virtual void run() = 0;

    public:
        const String getSID();
        const bool isActive();
        void execute();
};

#endif // SERVICE_H

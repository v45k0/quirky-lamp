#include "WatchdogService.h"

int WatchdogService::instances = 0;
WatchdogService::WatchdogService():
    Service(String(String("Watchdog ") +
                String(WatchdogService::instances++)).c_str(), true) {
    Log.debugln(this->getSID() + String(F(" initialized")));
    this->lastHeap = -1;
    this->lastTimestamp = 0;
    this->network = NetworkService::get();
}

void WatchdogService::run() {
    int usage = ESP.getFreeHeap();
    unsigned long timestamp = millis();

    if (this->lastHeap > 0) {
        int deltaHeap = usage - this->lastHeap;
        unsigned long deltaTime = timestamp - this->lastTimestamp;
        if (deltaHeap > 0) {
            Log.infoln(String(deltaHeap) + String(F(" Byte have been deallocated since "))
                    + String(deltaTime) + String(F(" ms ago")));
        } else if (deltaHeap < 0) {
            Log.infoln(String(deltaHeap) + String(F(" Byte have been allocated since "))
                    + String(deltaTime) + String(F(" ms ago")));
        } else {
            Log.infoln(String(F("No heap change since "))
                    + String(deltaTime) + String(F(" ms ago")));
        }
    } else {
        Log.infoln(String(F("Free heap = ")) + String(usage) + String(F(" Byte")));
    }

    this->network->sendPacket(String(F("/mem")), String(usage));
    this->lastHeap = usage;
    this->lastTimestamp = timestamp;
}

#ifndef __QL_WATCHDOG_SERVICE_H__
#define __QL_WATCHDOG_SERVICE_H__

#include <Esp.h>

#include "Log.h"
#include "Service.h"
#include "Services/NetworkService.h"

class WatchdogService : public Service {
    private:
        int lastHeap;
        unsigned long lastTimestamp;
        NetworkService* network;
        static int instances;

    protected:
        void run() override final;

    public:
        WatchdogService();
};

#endif // __QL_WATCHDOG_SERVICE_H__

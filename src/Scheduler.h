#ifndef __QL_SCHEDULER_H__
#define __QL_SCHEDULER_H__

#include <queue>
#include "Log.h"
#include "Task.h"
#include "Services/Service.h"

class Scheduler {
    private:
        std::priority_queue <Task, std::vector<Task>> tasks;

        Scheduler();

        static Scheduler *singleton;

    public:
        void run();
        void addTask(Service *service);
        void addTask(Service *service, unsigned long period);
        static Scheduler* get();
};

#endif // __QL_SCHEDULER_H__

#include "Log.h"

constexpr char LogClass::INFO[];
constexpr char LogClass::WARN[];
constexpr char LogClass::DEBUG[];
constexpr char LogClass::SEPARATOR[];

void LogClass::infoSeparator() {
#ifdef LOG_INFO
    Serial.print(String(LogClass::INFO));
    Serial.println(String(LogClass::SEPARATOR));
#endif
}

void LogClass::warningSeparator() {
#ifdef LOG_WARNING
    Serial.print(String(LogClass::WARN));
    Serial.println(String(LogClass::SEPARATOR));
#endif
}

void LogClass::debugSeparator() {
#ifdef LOG_DEBUG
    Serial.print(String(LogClass::DEBUG));
    Serial.println(String(LogClass::SEPARATOR));
#endif
}

LogClass Log;

#include "Scheduler.h"

void Scheduler::run() {
    if (this->tasks.empty()) {
        return;
    }

    Task current = this->tasks.top();

    if (current.deadline > millis()) {
        return;
    }

    Log.debugln(String("Executing ") + current.service->getSID()
            + String(" ") + String(millis()) + String(" ms"));
    this->tasks.pop();
    current.service->execute();

    if (! current.oneShot) {
        current.computeNextDeadline();
        this->tasks.push(current);
    }
}

void Scheduler::addTask(Service* service) {
    Log.debugln(String(F("Added one-shot scheduler task: ")) + service->getSID());
    Task task(service, true);
    this->tasks.push(task);
}

void Scheduler::addTask(Service* service, unsigned long period) {
    Log.debugln(String(F("Added scheduler task ")) + service->getSID()
            + String(F(" with period = ")) + String(period) + String("ms"));
    Task task(service, period);
    this->tasks.push(task);
}

Scheduler::Scheduler(){}

Scheduler* Scheduler::singleton = NULL;
Scheduler* Scheduler::get() {
    if (Scheduler::singleton == NULL) {
        Log.debugln(F("Created the scheduler singleton!"));
        Scheduler::singleton = new Scheduler();
    }
    return Scheduler::singleton;
}

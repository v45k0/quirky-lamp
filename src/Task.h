#ifndef __QL_TASK_H__
#define __QL_TASK_H__

#include <Arduino.h>
#include "Services/Service.h"

class Task {
    private:
        Task(Service *service, unsigned long period,
                unsigned long deadline, bool oneShot);

    protected:
        bool oneShot;
        Service *service;
        unsigned long period;
        unsigned long deadline;

        Task(Service *service, bool oneShot);
        Task(Service *service, unsigned long period);
        Task(Service *service, unsigned long period, unsigned long deadline);

        void computeNextDeadline();
        friend class Scheduler;

    public:
        bool operator < (const Task &other) const;
        bool operator > (const Task &other) const;
        bool operator >= (const Task &other) const;
        bool operator <= (const Task &other) const;
        bool operator == (const Task &other) const;
};

#endif // __QL_TASK_H__

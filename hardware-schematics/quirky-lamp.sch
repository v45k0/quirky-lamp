EESchema Schematic File Version 2
LIBS:lcd16x2
LIBS:ky037_sound_sensor
LIBS:ws2812b_led_strip
LIBS:tmp-36gz
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:esp32_core_board_v2
LIBS:sensors
LIBS:switches
LIBS:quirky-lamp-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L TMP-36GZ U?
U 1 1 5A4E6103
P 3450 1900
F 0 "U?" H 3200 2150 50  0000 C CNN
F 1 "TMP-36GZ" H 3500 2150 50  0000 L CNN
F 2 "" H 3500 1650 50  0001 L CNN
F 3 "" H 3450 1900 50  0001 C CNN
	1    3450 1900
	0    1    1    0   
$EndComp
$Comp
L SW_Push SubmodeSettings
U 1 1 5A4E7555
P 7850 3000
F 0 "SubmodeSettings" H 7900 3100 50  0000 L CNN
F 1 "SW_Push" H 7850 2940 50  0000 C CNN
F 2 "" H 7850 3200 50  0001 C CNN
F 3 "" H 7850 3200 50  0001 C CNN
	1    7850 3000
	1    0    0    -1  
$EndComp
$Comp
L ESP32_Core_Board_V2 U?
U 1 1 5A4EDA3E
P 5950 2900
F 0 "U?" H 5950 3750 60  0000 C CNN
F 1 "ESP32_Core_Board_V2" V 5950 2850 60  0000 C CNN
F 2 "" H 5950 2900 60  0001 C CNN
F 3 "" H 5950 2900 60  0001 C CNN
	1    5950 2900
	1    0    0    -1  
$EndComp
$Comp
L KY037_Sound_Sensor Sound_Sensor
U 1 1 5A4EDEC9
P 4450 1050
F 0 "Sound_Sensor" V 4200 1050 60  0000 C CNN
F 1 "KY037" H 4500 1350 47  0000 C CNN
F 2 "" H 5000 800 60  0001 C CNN
F 3 "" H 5000 800 60  0001 C CNN
	1    4450 1050
	0    1    1    0   
$EndComp
$Comp
L WS2812B_LED_Strip Addressable_LED_Strip
U 1 1 5A4EE835
P 9600 1650
F 0 "Addressable_LED_Strip" H 9100 1700 60  0000 C CNN
F 1 "WS2812B" H 9850 1900 51  0000 C CNN
F 2 "" H 9900 1400 60  0001 C CNN
F 3 "" H 9900 1400 60  0001 C CNN
	1    9600 1650
	-1   0    0    -1  
$EndComp
NoConn ~ 5250 2000
NoConn ~ 5250 2100
NoConn ~ 5250 2200
NoConn ~ 3950 3700
NoConn ~ 3950 3600
NoConn ~ 3950 3500
NoConn ~ 3950 3400
$Comp
L +5V #PWR?
U 1 1 5A4F55A8
P 5900 4350
F 0 "#PWR?" H 5900 4200 50  0001 C CNN
F 1 "+5V" H 5900 4490 50  0000 C CNN
F 2 "" H 5900 4350 50  0001 C CNN
F 3 "" H 5900 4350 50  0001 C CNN
	1    5900 4350
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR?
U 1 1 5A4F5F83
P 6000 4200
F 0 "#PWR?" H 6000 3950 50  0001 C CNN
F 1 "GND" H 6000 4050 50  0000 C CNN
F 2 "" H 6000 4200 50  0001 C CNN
F 3 "" H 6000 4200 50  0001 C CNN
	1    6000 4200
	1    0    0    -1  
$EndComp
NoConn ~ 3850 1100
NoConn ~ 4300 1550
NoConn ~ 4300 1700
NoConn ~ 4300 4000
NoConn ~ 5250 3700
NoConn ~ 5250 3600
NoConn ~ 5250 3500
$Comp
L POT POT
U 1 1 5A4FBDD2
P 4600 4500
F 0 "POT" V 4425 4500 50  0000 C CNN
F 1 "10k" V 4500 4500 50  0000 C CNN
F 2 "" H 4600 4500 50  0001 C CNN
F 3 "" H 4600 4500 50  0001 C CNN
	1    4600 4500
	0    -1   -1   0   
$EndComp
$Comp
L +5V #PWR?
U 1 1 5A4F672D
P 8650 1300
F 0 "#PWR?" H 8650 1150 50  0001 C CNN
F 1 "+5V" H 8650 1440 50  0000 C CNN
F 2 "" H 8650 1300 50  0001 C CNN
F 3 "" H 8650 1300 50  0001 C CNN
	1    8650 1300
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR?
U 1 1 5A4F6753
P 8650 2000
F 0 "#PWR?" H 8650 1750 50  0001 C CNN
F 1 "GND" H 8650 1850 50  0000 C CNN
F 2 "" H 8650 2000 50  0001 C CNN
F 3 "" H 8650 2000 50  0001 C CNN
	1    8650 2000
	0    1    1    0   
$EndComp
$Comp
L R R
U 1 1 5A4F85BE
P 4800 650
F 0 "R" V 4880 650 50  0000 C CNN
F 1 "10k" V 4800 650 50  0000 C CNN
F 2 "" V 4730 650 50  0001 C CNN
F 3 "" H 4800 650 50  0001 C CNN
	1    4800 650 
	0    1    1    0   
$EndComp
$Comp
L R_PHOTO R?
U 1 1 5A4F9DFD
P 4950 2200
F 0 "R?" V 4950 2150 50  0000 L CNN
F 1 "R_PHOTO" V 5000 2050 50  0000 L TNN
F 2 "" V 5000 1950 50  0001 L CNN
F 3 "" H 4950 2150 50  0001 C CNN
	1    4950 2200
	0    1    1    0   
$EndComp
NoConn ~ 5250 3400
$Comp
L R R
U 1 1 5A4FD9A1
P 7500 4050
F 0 "R" V 7580 4050 50  0000 C CNN
F 1 "10k" V 7500 4050 50  0000 C CNN
F 2 "" V 7430 4050 50  0001 C CNN
F 3 "" H 7500 4050 50  0001 C CNN
	1    7500 4050
	1    0    0    -1  
$EndComp
NoConn ~ 6650 3800
NoConn ~ 6650 3700
NoConn ~ 6650 3600
NoConn ~ 6650 3300
NoConn ~ 6650 2800
NoConn ~ 6650 2700
NoConn ~ 6650 2600
NoConn ~ 6650 2000
NoConn ~ 6650 2200
NoConn ~ 6650 2300
NoConn ~ 6650 2400
Text GLabel 10550 5250 2    60   Input ~ 0
GND
Text GLabel 10550 5600 2    60   Input ~ 0
+5V
Text GLabel 900  5600 0    60   Input ~ 0
+5V
Text GLabel 900  5250 0    60   Input ~ 0
GND
NoConn ~ 4300 4100
NoConn ~ 5250 5250
Text Notes 8150 7650 0    60   ~ 0
Jan 2018
Text Notes 7350 7500 0    60   ~ 0
Quirky Lamp Electronics Schematics
Text Notes 10600 7650 0    60   ~ 0
5
Text Notes 7050 6950 0    60   ~ 0
Quirky Lamp Project, Electronics Schematics\nProject Repository: https://gitlab.com/v45k0/quirky-lamp\nAuthor: Vasil Sarafov <contact@sarafov.net>, <sarafov@cs.tum.edu>
$Comp
L LCD16X2 LCD_Display
U 1 1 5A4EB888
P 3450 3550
F 0 "LCD_Display" H 2850 3950 50  0000 C CNN
F 1 "LCD16X2" H 4150 3950 50  0000 C CNN
F 2 "WC1602A" H 3450 3500 50  0001 C CIN
F 3 "" H 3450 3550 50  0001 C CNN
	1    3450 3550
	0    -1   -1   0   
$EndComp
NoConn ~ 6650 2500
$Comp
L R R
U 1 1 5A60BF6C
P 2700 3950
F 0 "R" V 2780 3950 50  0000 C CNN
F 1 "200" V 2700 3950 50  0000 C CNN
F 2 "" V 2630 3950 50  0001 C CNN
F 3 "" H 2700 3950 50  0001 C CNN
	1    2700 3950
	1    0    0    -1  
$EndComp
Text GLabel 900  5950 0    60   Input ~ 0
+5V
Text GLabel 10550 5950 2    60   Input ~ 0
+5V
NoConn ~ 2700 5250
NoConn ~ 2700 5600
NoConn ~ 2100 5250
NoConn ~ 4150 5250
NoConn ~ 4150 5600
NoConn ~ 8900 5250
NoConn ~ 8900 5600
Text Notes 700  7150 0    60   ~ 0
There are two power supplies with common ground:\n\n- The first one is for the ESP32 and the lamp switch,\n- The second one is for the LCD display \nthe LED strip and the sensors\n(separated from the ESP32 because of high current consumption)\n\nThe ESP32Dev Board is power hungry when booting/flashing
NoConn ~ 4600 1600
NoConn ~ 6650 2900
Wire Wire Line
	4300 1550 4300 2500
Wire Wire Line
	4300 2500 5250 2500
Wire Wire Line
	4600 1550 4600 2400
Wire Wire Line
	4600 2400 5250 2400
Wire Wire Line
	3950 3000 4450 3000
Wire Wire Line
	4450 3000 4450 2700
Wire Wire Line
	4450 2700 5250 2700
Wire Wire Line
	3950 3100 4550 3100
Wire Wire Line
	4550 3100 4550 2800
Wire Wire Line
	4550 2800 5250 2800
Wire Wire Line
	3950 3200 4650 3200
Wire Wire Line
	4650 3200 4650 2900
Wire Wire Line
	4650 2900 5250 2900
Wire Wire Line
	3950 3300 4750 3300
Wire Wire Line
	4750 3300 4750 3000
Wire Wire Line
	4750 3000 5250 3000
Wire Bus Line
	900  5600 10550 5600
Wire Wire Line
	3950 4300 4050 4300
Wire Wire Line
	4050 4300 4050 5250
Wire Wire Line
	3950 4200 4150 4200
Connection ~ 4050 5250
Connection ~ 3150 1100
Wire Wire Line
	3150 1900 3150 1100
Wire Wire Line
	4000 1550 4400 1550
Wire Wire Line
	4000 650  4000 1550
Wire Wire Line
	2300 1100 4000 1100
Connection ~ 2800 5250
Connection ~ 3850 1700
Wire Wire Line
	3850 1900 3750 1900
Wire Wire Line
	3850 900  3850 1900
Wire Wire Line
	4500 1550 4500 1700
Wire Wire Line
	4500 1700 3850 1700
Wire Wire Line
	2100 900  3850 900 
Wire Wire Line
	3950 3900 4300 3900
Wire Wire Line
	4300 3900 4300 5250
Connection ~ 4300 5250
Wire Wire Line
	5250 3300 5100 3300
Wire Wire Line
	5100 3300 5100 5250
Connection ~ 5100 5250
Wire Wire Line
	5250 3800 5250 5600
Connection ~ 5250 5600
Wire Wire Line
	8650 1300 8650 1550
Wire Wire Line
	8650 1550 8950 1550
Wire Wire Line
	8650 2000 8650 1750
Wire Wire Line
	8650 1750 8950 1750
Wire Wire Line
	7250 1650 8950 1650
Wire Wire Line
	5900 4350 5900 4100
Wire Wire Line
	6000 4200 6000 4100
Wire Bus Line
	900  5250 10550 5250
Connection ~ 7500 5250
Wire Wire Line
	3950 2800 3950 2650
Wire Wire Line
	3950 2650 2800 2650
Wire Wire Line
	2800 2650 2800 5250
Wire Wire Line
	3950 2900 4050 2900
Wire Wire Line
	4050 2900 4050 2550
Wire Wire Line
	4050 2550 2700 2550
Wire Wire Line
	2700 2550 2700 3800
Wire Wire Line
	3450 2300 3450 2450
Wire Wire Line
	3450 2450 4200 2450
Wire Wire Line
	4200 2450 4200 2600
Wire Wire Line
	4200 2600 5250 2600
Wire Wire Line
	2300 1100 2300 5250
Wire Wire Line
	2100 900  2100 5950
Connection ~ 2300 5250
Wire Bus Line
	900  5950 10550 5950
Wire Wire Line
	2700 4100 2700 5950
Connection ~ 2700 5950
Wire Wire Line
	4150 4200 4150 5950
Connection ~ 4150 5950
Wire Wire Line
	8700 1750 8700 5250
Connection ~ 8700 1750
Connection ~ 8700 5250
Wire Wire Line
	8900 1550 8900 5950
Connection ~ 8900 1550
Connection ~ 8900 5950
Wire Wire Line
	5150 2300 5250 2300
Wire Wire Line
	5150 650  5150 2300
Wire Wire Line
	5150 2200 5100 2200
Wire Wire Line
	5150 650  4950 650 
Connection ~ 5150 2200
Wire Wire Line
	4650 650  4000 650 
Connection ~ 4000 1100
Wire Wire Line
	4800 2200 4750 2200
Wire Wire Line
	4750 2200 4750 1600
Wire Wire Line
	4750 1600 4500 1600
Connection ~ 4500 1600
Wire Wire Line
	6650 3000 7650 3000
Wire Wire Line
	7500 3000 7500 3900
Connection ~ 7500 3000
Wire Wire Line
	7500 4200 7500 5250
Wire Wire Line
	7250 1650 7250 3100
Wire Wire Line
	7250 3100 6650 3100
NoConn ~ 7250 3000
NoConn ~ 5250 3100
NoConn ~ 5250 3200
Wire Wire Line
	3950 3800 4950 3800
Wire Wire Line
	4950 3800 4950 4600
Wire Wire Line
	4950 4600 6800 4600
Wire Wire Line
	6800 4600 6800 3400
Wire Wire Line
	6800 3400 6650 3400
Wire Wire Line
	3950 4000 4800 4000
Wire Wire Line
	4800 4000 4800 4800
Wire Wire Line
	4800 4800 7000 4800
Wire Wire Line
	7000 4800 7000 3200
Wire Wire Line
	7000 3200 6650 3200
Wire Wire Line
	4450 4500 4450 5250
Connection ~ 4450 5250
Wire Wire Line
	3950 4100 4600 4100
Wire Wire Line
	4600 4100 4600 4350
Wire Wire Line
	4750 4500 4750 5950
Connection ~ 4750 5950
NoConn ~ 4750 5250
NoConn ~ 4750 5600
Text GLabel 6800 3400 2    31   Input ~ 0
LCD_E6
Text GLabel 7000 3200 2    31   Input ~ 0
LCD_RS4
NoConn ~ 6650 2100
Wire Wire Line
	8050 3000 8500 3000
Wire Wire Line
	8500 3000 8500 5600
Connection ~ 8500 5600
NoConn ~ 8500 5250
NoConn ~ 2100 5600
Connection ~ 2100 5950
$EndSCHEMATC
